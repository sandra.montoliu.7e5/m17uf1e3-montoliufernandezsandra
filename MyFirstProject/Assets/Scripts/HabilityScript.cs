using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabilityScript : MonoBehaviour
{
    private float _counter = 0;
    public void StartHability()
    {
        GameObject.Find("Female_01").GetComponent<PlayerData>().StartHability = true;
        GameObject.Find("ButtonHability").GetComponent<Button>().interactable = false;
    }
}
