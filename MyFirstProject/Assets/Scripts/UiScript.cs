using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiScript : MonoBehaviour
{

    public PlayerData playerData;

    // Start is called before the first frame update
    void Start()
    {

        //this.gameObject.GetComponent<Text>().text = PlayerData.FindObjectOfType<PlayerData>().PlayerName; //Ens retorna el primer objecte amb el Script PlayerData que troba.
        //this.gameObject.GetComponent<Text>().text = GameObject.Find("Female_01").GetComponent<PlayerData>().PlayerName; //Igual que el de arriba, pero buscas el objeto especifico a través del nombre.

        this.gameObject.GetComponent<Text>().text = playerData.PlayerName;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
