using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using Unity.VisualScripting;
using UnityEngine.UI;

public enum PlayerType
{
    Priest,
    Warrior,
    Guard
}
public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    public float Heigh = 150f;
    public float Weight;
    [SerializeField]
        private int _yearsOld = 20;
    public int[] Stats;
    [SerializeField]
        private Sprite _sprite;
    public Transform MyTransform;
    
    //
    public Sprite[] Sprite;
    private int _indexSprite = 0;
    private int _count = 0;
    [SerializeField]
    private int _frameRate = 12; 
    private float _positionMultiplier = 0.003f;

    public PlayerType _playerType;

    public float Speed;
    public float Distance;
    private float _timePatrulla = 3;

    private bool irVolver = true;
    
    private Vector3 _lastPosition = new Vector3(0,0,0);

    private float _maxHeight = 500f;
    private float _minHeight = 150f;
    private bool _creixer = true;
    public bool StartHability = false;
    public float timeHability;
    private float _counter = 0;


    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Stats[0]);
        //Debug.Log(transform.position + " " + transform.rotation);
        //Debug.Log(transform.name);
        //Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().sprite.name);
        DontDestroyOnLoad(this.gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        // Animació
        Animation();

        // Moviment personatge
        if (EditorSceneManager.GetActiveScene() == EditorSceneManager.GetSceneByName("SampleScene"))
        {
            if (Input.GetAxis("Horizontal") != 0)
            {
                ManualControlPlayer();
            }
            else
            {
                switch (_playerType)
                {
                    case PlayerType.Guard:
                        Patrulla();
                        break;
                }
            }
                
        }

        // Habilitat gegantina
        CalculateHeight();
        if (StartHability == true) HabilitatGegantina();
    }

    void Animation()
    {
        if (_lastPosition != this.gameObject.transform.position)
        {
            if (_count >= _frameRate)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite[_indexSprite];
                _indexSprite++;
                if (_indexSprite > Sprite.Length - 1) _indexSprite = 0;
                _count = 0;
            }
            _count++;
        }
        _lastPosition = this.gameObject.transform.position;

        if (transform.position.x >= Distance || transform.position.x <= -Distance)
        {
            // Torna a la posició inicial
            //GameObject.Find("Female_01").GetComponent<SpriteRenderer>().sprite = Sprite[1];
        }
    }

    void Patrulla()
    { 
        if (irVolver == true)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Distance, 0, 0), Speed * Time.deltaTime);

            if (transform.position.x >= Distance)
            {
                if (_counter < _timePatrulla)
                {
                    _counter += Time.deltaTime;
                    // Torna a la posició inicial
                    //GameObject.Find("Female_01").GetComponent<SpriteRenderer>().sprite = Sprite[1];
                }
                else
                {
                    irVolver = false;
                    _counter = 0;
                }
                
            }
        }
        else if (irVolver == false)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-Distance, 0, 0), Speed * Time.deltaTime);

            if (transform.position.x <= -Distance)
            {
                if (_counter < _timePatrulla)
                {
                    _counter += Time.deltaTime;
                    // Torna a la posició inicial
                    //GameObject.Find("Female_01").GetComponent<SpriteRenderer>().sprite = Sprite[1];
                }
                else
                {
                    irVolver = true;
                    _counter = 0;
                }
            }
        }

    }

    void ManualControlPlayer()
    {
        transform.position = new Vector3(_positionMultiplier * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z); // Cambiar X
        //transform.position = new Vector3(transform.position.x, _positionMultiplier * Input.GetAxis("Vertical") + transform.position.y, transform.position.z); // Cambiar Y

        // Girar el Sprite en el moviment manual
        if (Input.GetAxis("Horizontal") > 0) this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
        else this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
    }

    void CalculateSpeed()
    {

    }
   
    void CalculateHeight()
    {
        this.gameObject.transform.localScale = new Vector3(gameObject.transform.GetComponent<PlayerData>().Heigh / 150, gameObject.transform.GetComponent<PlayerData>().Heigh / 150,1);
    }

    public void HabilitatGegantina()
    {
        if (_creixer == true)
        {
            // El personatge va creixent fins arribar a la maxHeight
            gameObject.transform.GetComponent<PlayerData>().Heigh++;
            if (gameObject.transform.GetComponent<PlayerData>().Heigh >= _maxHeight)
            {
                _creixer = false;
            }
        }
        else if (gameObject.transform.GetComponent<PlayerData>().Heigh >= _maxHeight)
        {
            // Mante la habilitat activada per un tems determinat.
            if (_counter < timeHability) _counter += Time.deltaTime;
            else gameObject.transform.GetComponent<PlayerData>().Heigh--;
        }
        else if (_creixer == false && gameObject.transform.GetComponent<PlayerData>().Heigh > _minHeight)
        {
            // El personatge decreix fins arribar a la minHeight
            if (_counter != 0) _counter = 0;
            gameObject.transform.GetComponent<PlayerData>().Heigh--;
        }
        else if (gameObject.transform.GetComponent<PlayerData>().Heigh <= _minHeight)
        {
            // El botó te un temps d'espera (segons el temps que duri l'habilitat) abans de poder activar-lo de nou.
            if (_counter < timeHability) _counter += Time.deltaTime;
            else
            {
                _creixer = true;
                StartHability = false;
                _counter = 0;
                GameObject.Find("ButtonHability").GetComponent<Button>().interactable = true;
            }
        }
    }
}
