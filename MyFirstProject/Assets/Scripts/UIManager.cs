using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private float _time;
    private float _newTime;
    private void Start()
    {
        _time = Time.time;
        GameObject.Find("SliderDistance").GetComponent<Slider>().value = GameObject.Find("Female_01").GetComponent<PlayerData>().Distance;
        GameObject.Find("SliderSpeed").GetComponent<Slider>().value = GameObject.Find("Female_01").GetComponent<PlayerData>().Speed;
        GameObject.Find("SliderHabilityTime").GetComponent<Slider>().value = GameObject.Find("Female_01").GetComponent<PlayerData>().timeHability;
    }

    private void Update()
    {
        // Calcular Frames per segon.
        _newTime = Time.time;
        if (1f <= _newTime - _time)
        {
            float freamSeconds = Mathf.RoundToInt(1.0f / Time.deltaTime);
            GameObject.Find("Frame").transform.GetComponent<Text>().text = freamSeconds + " Frames / Second";
            _time = _newTime;
        }

        ChangeDistance();
        ChangeSpeed();
        ChangeHabilityTime();
    }

    public void HideGameStats()
    {
        GameObject.Find("GameStats").SetActive(false);       
    }

    void ChangeDistance()
    {
        GameObject.Find("Female_01").GetComponent<PlayerData>().Distance = GameObject.Find("SliderDistance").GetComponent<Slider>().value;
        GameObject.Find("Distance").GetComponent<Text>().text = "Distancia\n" + GameObject.Find("SliderDistance").GetComponent<Slider>().value;
    }

    void ChangeSpeed()
    {
        GameObject.Find("Female_01").GetComponent<PlayerData>().Speed = GameObject.Find("SliderSpeed").GetComponent<Slider>().value;
        GameObject.Find("Speed").GetComponent<Text>().text = "Velocitat\n" + GameObject.Find("SliderSpeed").GetComponent<Slider>().value;
    }

    void ChangeHabilityTime()
    {
        GameObject.Find("Female_01").GetComponent<PlayerData>().timeHability = GameObject.Find("SliderHabilityTime").GetComponent<Slider>().value;
        GameObject.Find("HabilityTime").GetComponent<Text>().text = "Time Hability\n" + GameObject.Find("SliderHabilityTime").GetComponent<Slider>().value + " s";
    }


}

