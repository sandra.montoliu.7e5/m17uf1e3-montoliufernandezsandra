using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        // Cambiar d'escena a través dels botons.
        SceneManager.LoadScene(sceneName);
    }
}
